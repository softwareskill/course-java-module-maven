package pl.softwareskill.course.maven.credit;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CreditCalculatorTest {

    @Test
    public void calculatesTotalPayoffForYear() {
        // given
        CreditParameters creditParameters = givenDefaultCreditParameters()
                .durationInMonths(12)
                .build();

        // when
        BigDecimal totalPayoff = CreditCalculator.calculateTotalPayoffAmount(creditParameters);

        // then
        assertThat(totalPayoff)
                .isEqualTo(BigDecimal.valueOf(10_272.84));
    }

    @Test
    public void calculatesTotalPayoffForTwoYears() {
        // given
        CreditParameters creditParameters = givenDefaultCreditParameters()
                .durationInMonths(24)
                .build();

        // when
        BigDecimal totalPayoff = CreditCalculator.calculateTotalPayoffAmount(creditParameters);

        // then
        assertThat(totalPayoff)
                .isEqualTo(BigDecimal.valueOf(10_529.04));
    }

    @Test
    public void calculatesTotalPayoffForHalfYear() {
        // given
        CreditParameters creditParameters = givenDefaultCreditParameters()
                .durationInMonths(6)
                .build();

        // when
        BigDecimal totalPayoff = CreditCalculator.calculateTotalPayoffAmount(creditParameters);

        // then
        assertThat(totalPayoff)
                .isEqualTo(BigDecimal.valueOf(10_146.36));
    }

    @Test
    public void calculatesTotalPayoffForYearWithCommission() {
        // given
        CreditParameters creditParameters = givenDefaultCreditParameters()
                .commission(BigDecimal.valueOf(100))
                .build();

        // when
        BigDecimal totalPayoff = CreditCalculator.calculateTotalPayoffAmount(creditParameters);

        // then
        assertThat(totalPayoff)
                .isEqualTo(BigDecimal.valueOf(10_375.68));
    }

    private static CreditParameters.CreditParametersBuilder givenDefaultCreditParameters() {
        return CreditParameters.builder()
                .amount(BigDecimal.valueOf(10_000))
                .interestRate(BigDecimal.valueOf(0.05))
                .durationInMonths(12)
                .commission(BigDecimal.ZERO);
    }
}