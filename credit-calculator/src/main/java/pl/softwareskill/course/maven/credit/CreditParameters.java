package pl.softwareskill.course.maven.credit;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class CreditParameters {

    private final BigDecimal amount;
    private final BigDecimal interestRate;
    private final int durationInMonths;
    private final BigDecimal commission;
}
