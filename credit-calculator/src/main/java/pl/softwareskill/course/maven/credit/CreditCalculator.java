package pl.softwareskill.course.maven.credit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CreditCalculator {

    private static final BigDecimal MONTHS_IN_YEAR = BigDecimal.valueOf(12);

    public static BigDecimal calculateTotalPayoffAmount(CreditParameters creditParameters) {
        BigDecimal installment = calculateMonthlyInstallment(creditParameters);
        return installment.multiply(BigDecimal.valueOf(creditParameters.getDurationInMonths()));
    }

    private static BigDecimal calculateMonthlyInstallment(CreditParameters creditParameters) {
        BigDecimal totalAmount = calculateTotalAmount(creditParameters);

        BigDecimal totalPercent = BigDecimal.ZERO;
        BigDecimal monthlyPercent = creditParameters.getInterestRate().divide(MONTHS_IN_YEAR, 10, BigDecimal.ROUND_HALF_UP);

        for (int i = 1; i <= creditParameters.getDurationInMonths(); i++) {
            BigDecimal partialPercent = BigDecimal.ONE
                    .add(monthlyPercent)
                    .pow(i);

            totalPercent = totalPercent.add(BigDecimal.ONE.divide(partialPercent, 10, BigDecimal.ROUND_HALF_UP));
        }

        return totalAmount.divide(totalPercent, 2, RoundingMode.HALF_UP);
    }

    private static BigDecimal calculateTotalAmount(CreditParameters creditParameters) {
        return creditParameters.getAmount().add(creditParameters.getCommission());
    }
}
