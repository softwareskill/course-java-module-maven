# part1

```
mvn archetype:generate -DgroupId=pl.softwareskill.course.maven -DartifactId=app-maven -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```

```
mvn package
```

```
java -cp target/app-maven-0.0.0-SNAPSHOT.jar pl.softwareskill.course.maven.App
```

# part3

```
mvn archetype:generate -DgroupId=pl.softwareskill.course.maven.credit -DartifactId=credit-calculator -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```

# part6

```
mvn archetype:generate -DgroupId=pl.softwareskill.course.maven.creditoffer -DartifactId=parent-project -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
cd parent-project

mvn archetype:generate -DgroupId=pl.softwareskill.course.maven.creditoffer -DartifactId=web -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false

mvn archetype:generate -DgroupId=pl.softwareskill.course.maven.creditoffer -DartifactId=cli -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false


mvn archetype:generate -DgroupId=pl.softwareskill.course.maven.creditoffer -DartifactId=core -Dversion=0.0.0-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false

```

# part 8

```
java -cp target/app-maven-0.0.0-SNAPSHOT.jar pl.softwareskill.course.maven.App
```

```
java -cp target/app-maven-0.0.0-SNAPSHOT-jar-with-dependencies.jar pl.softwareskill.course.maven.App
```

# part 13

```
mvn deploy -DskipTests
```

# part 14

```
mvn -B release:clean release:prepare release:perform -DreleaseVersion=0.0.0 -DdevelopmentVersion=0.1.0-SNAPSHOT
```