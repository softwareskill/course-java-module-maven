package pl.softwareskill.course.maven;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StringUtilsTest {

    @Test
    public void formatsToUpperCase() {
        String input = " Thats my string ";

        String output = StringUtils.title(input);

        assertThat(output)
                .isEqualTo("THATS MY STRING");
    }
}
